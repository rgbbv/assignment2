package bgu.spl.mics.application.messages;

import bgu.spl.mics.Event;
import bgu.spl.mics.application.passiveObjects.Customer;

public class BookOrderEvent implements Event {
    private String bookName;
    private Customer customer;
    private int orderTick;

    /**
     * @return the name of the book
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * @return the customer that ordered the book
     */
    public Customer getCustomer() {return customer; }

    public BookOrderEvent(String bookName, Customer customer, int orderTick) {
        this.bookName = bookName;
        this.customer = customer;
        this.orderTick = orderTick;
    }

    /**
     * @return the tick in which the order was made
     */
    public int getOrderTick() {
        return orderTick;
    }
}
