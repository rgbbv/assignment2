package bgu.spl.mics.application.messages;

import bgu.spl.mics.Event;

public class DeliveryEvent implements Event {

    private String address;
    private int distance;

    public DeliveryEvent(String address, int distance) {
        this.address = address;
        this.distance = distance;
    }

    /**
     * @return the address to deliver the book to
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return the distance to the address in KM
     */
    public int getDistance() {
        return distance;
    }
}
