package bgu.spl.mics.application.messages;

import bgu.spl.mics.Event;
import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;

public class ReleaseVehicleEvent implements Event<DeliveryVehicle> {

    private DeliveryVehicle releasedVehicle;

    public ReleaseVehicleEvent(DeliveryVehicle vehicle) { this.releasedVehicle = vehicle; }

    /**
     * @return the vehicle after he finished the delivery
     */
    public DeliveryVehicle getReleasedVehicle() {
        return releasedVehicle;
    }
}
