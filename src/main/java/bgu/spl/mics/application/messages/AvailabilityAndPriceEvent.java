package bgu.spl.mics.application.messages;

import bgu.spl.mics.Event;
import javafx.util.Pair;

public class AvailabilityAndPriceEvent implements Event<Pair<Boolean,Integer>> {
    private String bookName;


    /**
     * @param bookName the book we want to receive information about
     */
    public AvailabilityAndPriceEvent(String bookName){
        this.bookName = bookName;
    }

    /**
     * @return the name of the book
     */
    public String getBookName(){
        return bookName;
    }
}
