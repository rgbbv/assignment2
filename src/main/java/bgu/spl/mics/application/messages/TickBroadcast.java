package bgu.spl.mics.application.messages;

import bgu.spl.mics.Broadcast;

public class TickBroadcast implements Broadcast {

    private int tick;

    public TickBroadcast (int tick) {
        this.tick = tick;
    }

    /**
     * @return the current tick of the program
     */
    public int getTick(){
        return tick;
    }
}
