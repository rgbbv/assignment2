package bgu.spl.mics.application.messages;

import bgu.spl.mics.Event;

public class RemoveBookEvent implements Event {

    private String bookTitle;

    public RemoveBookEvent(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    /**
     * @return the name of the book we want to remove from inventory
     */
    public String getBookTitle() {
        return bookTitle;
    }
}
