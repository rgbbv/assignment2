package bgu.spl.mics.application;

import bgu.spl.mics.application.passiveObjects.*;
import bgu.spl.mics.application.services.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/** This is the Main class of the application. You should parse the input file,
 * create the different instances of the objects, and run the system.
 * In the end, you should output serialized objects.
 */
public class BookStoreRunner {
    static public CountDownLatch latch;

    public static void main(String[] args) {
        BookStoreRunner runner = new BookStoreRunner();
        String inputInJson = null;
        try {
            inputInJson = runner.readFile(args[0]);  //getting the input string, which is currently in Json format
        } catch (IOException e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        Type founderListType = new TypeToken<InputInfo>(){}.getType();

        InputInfo info = gson.fromJson(inputInJson,founderListType);  //converting the string containing the input as Json to an object, which we can retrieve the info from easily


        int numOfThreads = getNumOfThreads(info);
        ExecutorService e = Executors.newFixedThreadPool(numOfThreads);

        latch = new CountDownLatch(numOfThreads - 1);


        Inventory inventory = Inventory.getInstance();
        inventory.load(info.getInitialInventory());   //init the singleton inventory with bookinfo

        ResourcesHolder resourcesHolder = ResourcesHolder.getInstance();
        resourcesHolder.load(info.getDeliveryVehicles()); //init the resourceholder singleton with cars

        TimeService timeTemp = info.getServices().getTime(); //init the timeService
        TimeService timeService = new TimeService(timeTemp.getSpeed(), timeTemp.getDuration()); //making a new one to get the messagebus singleton instance

        //init the api service with orders
        Customer[] customers = info.getServices().getCustomers();
        runner.executeApiService(e, customers);


        //init the other services
        runner.executeSimpleServices(info, e);


        try { //waiting for all services to initialise before starting timeService
            latch.await();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        e.execute(timeService);  //the last thread to start running

        runner.closeAll(e);

        //writing outputs to file
        //output for Customers
        runner.printCustomersToFile(customers,args[1]);
        //output for book_amount Hashmap
        inventory.printInventoryToFile(args[2]);
        //output for orderReceipts
        MoneyRegister register = MoneyRegister.getInstance();
        register.printOrderReceipts(args[3]);
        //output for moneyregister
        register.printRegister(args[4]);
    }

    /**
     * init and execute the api service
     * @param e the executorService which run the service
     * @param customers the list of customers the ApiService need to be initialised
     */
    private void executeApiService(ExecutorService e, Customer[] customers) {
        List<Customer.CustomerOrder> orders = new ArrayList<>();

        for(Customer customer: customers){
            customer.organizeOrders();
            Customer.CustomerOrder[] customerOrders = customer.getOrderSchedule();
            for (Customer.CustomerOrder order: customerOrders){
                orders.add(order);
            }
        }
        APIService apiService = new APIService(orders); //init the api service with the customers' orders
        e.execute(apiService);
    }

    /**
     * @param info the json object to retrieve the info from
     * @return the num of threads need to be init.
     */
    private static int getNumOfThreads(InputInfo info) {
        int numOfThreads = 2 + info.getServices().getInventoryService(); //+2 cause of time and api services
        numOfThreads += info.getServices().getLogistics();
        numOfThreads += info.getServices().getResourcesService();
        numOfThreads += info.getServices().getSelling();
        return numOfThreads;
    }

    /**
     * @param info the json object to pull relevant info from
     * @param e the executor service to execute all the services
     */
    private void executeSimpleServices(InputInfo info, ExecutorService e) {
        for (int i = 0; i < info.getServices().getSelling(); i++) { //init X amount of selling services where X is given in inputinfo
            SellingService sell = new SellingService();
            e.execute(sell);
        }
        for (int i = 0; i < info.getServices().getInventoryService(); i++) { //init X amount of inventory services where X is given in inputinfo
            InventoryService inv = new InventoryService();
            e.execute(inv);
        }
        for (int i = 0; i < info.getServices().getLogistics(); i++) { //init X amount of logistics services where X is given in inputinfo
            LogisticsService log = new LogisticsService();
            e.execute(log);
        }
        for (int i = 0; i < info.getServices().getResourcesService(); i++) { //init X amount of resources services where X is given in inputinfo
            ResourceService res = new ResourceService();
            e.execute(res);
        }
    }

    /**
     * @param e the executerService which issues thread to each service
     *          this method shuts down all the threads
     */
    private void closeAll(ExecutorService e) {
        //shutting down gracefully when number of passed ticks equal to duration
        e.shutdown(); //trying to shutdown the execservice, waiting for all threads to terminate.
        try {
            while (!e.awaitTermination(1, TimeUnit.SECONDS)) {
                continue;
            }
        } catch (InterruptedException ex) {
            e.shutdownNow();
            Thread.currentThread().interrupt();
            ex.printStackTrace();
        }
    }

    /**
     * @param customers an array of customers that should be printed
     * @param filename the name of the file this function write into
     */
    private void printCustomersToFile(Customer[] customers,String filename) {
        HashMap<Integer,Customer> toPrint = new HashMap<>();
        for (Customer customer : customers){
            toPrint.put(customer.getId(),customer);
        }

        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(toPrint);
            oos.close();
            fos.close();

        } catch (IOException ex){
            ex.printStackTrace();
        }
    }



    /**
     *
     * @param fileName the name of the file which we should read from
     * @return a String which is the text of the file filename
     * @throws IOException
     */

    public String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
}
