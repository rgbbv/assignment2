package bgu.spl.mics.application.passiveObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Passive data-object representing a customer of the store.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You may add fields and methods to this class as you see fit (including public methods).
 */
public class Customer implements Serializable {

	private int id;
	private String name;
	private String address;
	private int distance;
	private List<OrderReceipt> receipts;
	private CreditCard creditCard;
	private CustomerOrder[] orderSchedule;

	public Customer(int id, String name, String address, int distance,
					int creditCard, int availableAmountInCreditCard) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.distance = distance;
		receipts = Collections.synchronizedList(new ArrayList<>()); //wrapper which is thread-safe
		this.creditCard = new CreditCard(creditCard,availableAmountInCreditCard);
	}

	/**
	 * @return credit card of customer
	 */
	public CreditCard getCreditCard() {
		return creditCard;
	}

	/**
	 * @return the order schedule in a form of customerOrder array
	 */
	public CustomerOrder[] getOrderSchedule() {
		return orderSchedule;
	}

	/**
     * Retrieves the name of the customer.
     */
	public String getName() {
		return name;
	}

	/**
     * Retrieves the ID of the customer  . 
     */
	public int getId() {
		return id;
	}
	
	/**
     * Retrieves the address of the customer.  
     */
	public String getAddress() {
		return address;
	}
	
	/**
     * Retrieves the distance of the customer from the store.  
     */
	public int getDistance() {
		return distance;
	}

	
	/**
     * Retrieves a list of receipts for the purchases this customer has made.
     * <p>
     * @return A list of receipts.
     */
	public List<OrderReceipt> getCustomerReceiptList() {
		return receipts;
	}
	
	/**
     * Retrieves the amount of money left on this customers credit card.
     * <p>
     * @return Amount of money left.   
     */
	public int getAvailableCreditAmount() {
		return creditCard.getAmount();
	}
	
	/**
     * Retrieves this customers credit card serial number.    
     */
	public int getCreditNumber() {
		return creditCard.getNumber();
	}

	/**
	 * @param amount the amount of credit to charge the card
	 */
	public void chargeCreditCard(int amount) {

		creditCard.setAmount(creditCard.amount -= amount);
	}

	/**
	 * @param receipt add this receipt to the customer's receipt list
	 */
	public void addReceipt(OrderReceipt receipt) {
		if (receipts == null){
			receipts = Collections.synchronizedList(new ArrayList<>());
		}
		receipts.add(receipt);
	}

	/**
	 * @return return the instance of this customer
	 */
	private Customer getThisCustomer(){
		return this;
	}

	/**
	 * utility method to init the customerOrders correctly while using the json input file
	 * making the orderSchedule include Customer as its outer class
	 */
	public void organizeOrders () {
		for (int i = 0; i < orderSchedule.length; i++) {
			CustomerOrder temp = new CustomerOrder(orderSchedule[i].getBookTitle(), orderSchedule[i].getTick());
			orderSchedule[i] = temp;
		}
	}

	private class CreditCard implements Serializable {
		private int number;
		private int amount;

		/**
		 * @param number the credit card id number to set
		 */
		public void setNumber(int number) {
			this.number = number;
		}

		/**
		 * @param amount the amount of money to set in the card
		 */
		public void setAmount(int amount) {
			this.amount = amount;
		}

		/**
		 * @return get the card id number
		 */
		public int getNumber() {
			return number;
		}

		/**
		 * @return get the card current amount of money
		 */
		public int getAmount() {
			return amount;
		}

		public CreditCard(int number, int amount) {
			this.number = number;
			this.amount = amount;
		}

	}

	public class CustomerOrder implements Comparable<CustomerOrder>, Serializable{

		private String bookTitle;
		private int tick;

		public CustomerOrder(String bookTitle, int tick) {
			this.bookTitle = bookTitle;
			this.tick = tick;
		}

		/**
		 * @param bookTitle the title of the book to set
		 */
		public void setBookTitle(String bookTitle) {
			this.bookTitle = bookTitle;
		}

		/**
		 * @param tick set the tick of when the customer wants to order the book
		 */
		public void setTick(int tick) {
			this.tick = tick;
		}

		/**
		 * @return return the book's title
		 */
		public String getBookTitle() {
			return bookTitle;
		}

		/**
		 * @return return the tick which indicate when to buy the book
		 */
		public int getTick() {
			return tick;
		}

		/**
		 * @return get the customer instance associated to this order
		 */
		public Customer getCustomer() {
			return getThisCustomer() ;
		}

		/**
		 * @param order the other order which against we compare this order by tick
		 * @return positive integer if this order tick is larger then the other's,
		 *        0 if its the same tick,
		 *        negetive integer if the other tick is larger then this
		 */
		@Override
		public int compareTo(CustomerOrder order) {
			int myTick = this.getTick();
			int otherTick = order.getTick();

			return (myTick - otherTick);
		}
	}

}
