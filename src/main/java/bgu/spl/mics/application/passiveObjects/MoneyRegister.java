package bgu.spl.mics.application.passiveObjects;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Passive object representing the store finance management. 
 * It should hold a list of receipts issued by the store.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private fields and methods to this class as you see fit.
 */
public class MoneyRegister implements Serializable {


	private static class MoneyRegisterHolder {
		private static MoneyRegister instance = new MoneyRegister();
	}

	// the sum of all the earnings
	private AtomicInteger totalEarnings = new AtomicInteger(0);

	// saves all the receipts of past purchases
	private ConcurrentLinkedQueue<OrderReceipt> receipts = new ConcurrentLinkedQueue<>();

	/**
     * Retrieves the single instance of this class.
     */
	public static MoneyRegister getInstance() { return MoneyRegisterHolder.instance; }
	
	/**
     * Saves an order receipt in the money register.
     * <p>   
     * @param r		The receipt to save in the money register.
     */
	public void file (OrderReceipt r) {
		receipts.add(r);
	}
	
	/**
     * Retrieves the current total earnings of the store.  
     */
	public int getTotalEarnings() { return totalEarnings.get(); }
	
	/**
     * Charges the credit card of the customer a certain amount of money.
     * <p>
     * @param amount 	amount to charge
     */
	public void chargeCreditCard(Customer c, int amount) {
		boolean hasEnoughMoney = c.getAvailableCreditAmount() >= amount;
		if(hasEnoughMoney) {
			c.chargeCreditCard(amount);
			totalEarnings.addAndGet(amount);
		}
	}
	
	/**
     * Prints to a file named @filename a serialized object List<OrderReceipt> which holds all the order receipts 
     * currently in the MoneyRegister
     * This method is called by the main method in order to generate the output.. 
     */
	public void printOrderReceipts(String filename) {
		List<OrderReceipt> orderReceipts = new ArrayList<>();
		orderReceipts.addAll(receipts);
		try
		{
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(orderReceipts);
			oos.close();
			fos.close();
		}catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	/**
	 * @param filename the name of the file, which the program will write the register object into
	 */
	public void printRegister(String filename) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}
}
