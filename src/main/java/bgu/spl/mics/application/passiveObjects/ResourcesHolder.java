package bgu.spl.mics.application.passiveObjects;

import bgu.spl.mics.Future;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Passive object representing the resource manager.
 * You must not alter any of the given public methods of this class.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private methods and fields to this class.
 */
public class ResourcesHolder {

	private static class ResourcesHolderWrapper {
		private static ResourcesHolder instance = new ResourcesHolder();
	}

    //vehicles in use
	private List<DeliveryVehicle> occupiedVehicles = Collections.synchronizedList(new ArrayList<>());
    //vehicles waiting for instructions
    private List<DeliveryVehicle> freeVehicles = Collections.synchronizedList(new ArrayList<>());
	/**
     * Retrieves the single instance of this class.
     */
	public static ResourcesHolder getInstance() {
		return ResourcesHolderWrapper.instance;
	}
	
	/**
     * Tries to acquire a vehicle and gives a future object which will
     * resolve to a vehicle.
     * <p>
     * @return 	{@link Future<DeliveryVehicle>} object which will resolve to a 
     * 			{@link DeliveryVehicle} when completed.   
     */
	public Future<DeliveryVehicle> acquireVehicle() {
	    Future<DeliveryVehicle> future = new Future<DeliveryVehicle>();
	    findVehicle(future);
	    return future;
	}

    /**
     * @param future the future object which will be resolved with a vehicle if possible
     *               will be resolved to null if could not get a vehicle
     */
    private void findVehicle(Future<DeliveryVehicle> future) {
        Date date = new Date();

        long timeoutInMillis = 200;
        long nowInMillis = date.getTime();
        long endTimeInMillis = nowInMillis + timeoutInMillis;  //adding timeout to prevent deadlock of ResourceHolder,
        // that might happen if it is both waiting to a acquire a vehicle while also needing to release one

        synchronized (this) {
            while (freeVehicles.isEmpty()) { // checks if there are any free vehicles at the moment
                try {

                    // waits until releaseVehicle does notifyAll or timeout
                    wait(timeoutInMillis);

                    nowInMillis = new Date().getTime();
                    if (nowInMillis > endTimeInMillis) { // checks if I was woken by timeout
                        future.resolve(null);
                        return;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DeliveryVehicle newlyOccupied = freeVehicles.remove(0);
            occupiedVehicles.add(newlyOccupied);

            future.resolve(newlyOccupied);
        }

    }

    /**
     * Releases a specified vehicle, opening it again for the possibility of
     * acquisition.
     * <p>
     * @param vehicle	{@link DeliveryVehicle} to be released.
     */
	public void releaseVehicle(DeliveryVehicle vehicle) {
	    occupiedVehicles.remove(vehicle);
	    freeVehicles.add(vehicle);
	    synchronized (this) {
            notifyAll();
        }
	}
	
	/**
     * Receives a collection of vehicles and stores them.
     * <p>
     * @param vehicles	Array of {@link DeliveryVehicle} instances to store.
     */
	public void load(DeliveryVehicle[] vehicles) {
	    for (DeliveryVehicle vehicle : vehicles) {
	        freeVehicles.add(vehicle);
        }
	}
}
