package bgu.spl.mics.application.passiveObjects;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Passive data-object representing the store inventory.
 * It holds a collection of {@link BookInventoryInfo} for all the
 * books in the store.
 * <p>
 * This class must be implemented safely as a thread-safe singleton.
 * You must not alter any of the given public methods of this class.
 * <p>
 * You can add ONLY private fields and methods to this class as you see fit.
 */
public class Inventory {

	private static class InventoryHolder {
		private static Inventory instance = new Inventory();
	}
	// with the key (the name of the book) the hashMap returns the number left of this book
	private ConcurrentHashMap<String, BookInventoryInfo> name_bookInfo;

	/**
     * Retrieves the single instance of this class.
     */
	public static Inventory getInstance() {
		return InventoryHolder.instance;
	}

	private Inventory() {
		name_bookInfo = new ConcurrentHashMap<>();
	}
	
	/**
     * Initializes the store inventory. This method adds all the items given to the store
     * inventory.
     * <p>
     * @param inventory 	Data structure containing all data necessary for initialization
     * 						of the inventory.
     */
	public void load (BookInventoryInfo[ ] inventory ) {
		for (BookInventoryInfo book : inventory)
			name_bookInfo.put(book.getBookTitle(),book);
	}
	
	/**
     * Attempts to take one book from the store.
     * <p>
     * @param book 		Name of the book to take from the store
     * @return 	an {@link Enum} with options NOT_IN_STOCK and SUCCESSFULLY_TAKEN.
     * 			The first should not change the state of the inventory while the 
     * 			second should reduce by one the number of books of the desired type.
     */
	public OrderResult take (String book) {
		if (checkAvailabiltyAndGetPrice(book) == -1)
			return OrderResult.NOT_IN_STOCK;
		// decreases the amount in takenBook and puts it in the same entry
		name_bookInfo.computeIfPresent(book, (key,takenBook) -> takenBook.taken());
		return OrderResult.SUCCESSFULLY_TAKEN;
	}



	/**
     * Checks if a certain book is available in the inventory.
     * <p>
     * @param book 		Name of the book.
     * @return the price of the book if it is available, -1 otherwise.
     */
	public int checkAvailabiltyAndGetPrice(String book) {
		if (!name_bookInfo.containsKey(book) || name_bookInfo.get(book).getAmountInInventory() == 0)
			return -1;
		return name_bookInfo.get(book).getPrice();
	}
	
	/**
     * 
     * <p>
     * Prints to a file name @filename a serialized object HashMap<String,Integer> which is a Map of all the books in the inventory. The keys of the Map (type {@link String})
     * should be the titles of the books while the values (type {@link Integer}) should be
     * their respective available amount in the inventory. 
     * This method is called by the main method in order to generate the output.
     */
	public void printInventoryToFile(String filename){
		HashMap<String,Integer> toPrint = new HashMap();
		// enters the name and amount of each entry in name_bookInfo
		name_bookInfo.forEach((name,bookInfo)-> toPrint.put(name,bookInfo.getAmountInInventory()));
		try
		{
			FileOutputStream fos =
					new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(toPrint);
			oos.close();
			fos.close();
		}catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
}
