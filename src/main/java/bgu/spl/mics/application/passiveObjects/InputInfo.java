package bgu.spl.mics.application.passiveObjects;

import bgu.spl.mics.application.services.TimeService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

/**
 * the class which represents the json input object, from which we get all the
 * relevant input data for the program
 */
public class InputInfo {
    private BookInventoryInfo[] initialInventory;
    private JsonArray initialResources;
    private Services services;


    public class Services {
        private TimeService time;
        private int selling;
        private int inventoryService;
        private int logistics;
        private int resourcesService;
        private Customer[] customers;

        /**
         * @param time the timeservice to set as the program's time service
         */
        public void setTime(TimeService time) {
            this.time = time;
        }

        /**
         * @param selling  the amount of selling services to set
         */
        public void setSelling(int selling) {
            this.selling = selling;
        }

        /**
         * @param inventoryService the amount of inventory services to set
         */
        public void setInventoryService(int inventoryService) {
            this.inventoryService = inventoryService;
        }

        /**
         * @param logistics the amount of logisitic servcies to set
         */
        public void setLogistics(int logistics) {
            this.logistics = logistics;
        }

        /**
         * @param resourcesService the amount of resources services to set
         */
        public void setResourcesService(int resourcesService) {
            this.resourcesService = resourcesService;
        }

        /**
         * @param customers the customer's array to set
         */
        public void setCustomers(Customer[] customers) {
            this.customers = customers;
        }

        /**
         * @return the single time service of the program
         */
        public TimeService getTime() {
            return time;
        }

        /**
         * @return the amount of selling services of the program
         */
        public int getSelling() {
            return selling;
        }

        /**
         * @return the amount of inventory services of the program
         */
        public int getInventoryService() {
            return inventoryService;
        }

        /**
         * @return the amount of logistics services of the program
         */
        public int getLogistics() {
            return logistics;
        }

        /**
         * @return the amount of resources services of the program
         */
        public int getResourcesService() {
            return resourcesService;
        }

        /**
         * @return the customer array of the program
         */
        public Customer[] getCustomers() {
            return customers;
        }
    }

    /**
     * @return return an array of book info of the initial inventory
     */
    public BookInventoryInfo[] getInitialInventory() {
        return initialInventory;
    }

    /**
     * @return get the array of vehicles that are given as input
     */
    public DeliveryVehicle[] getDeliveryVehicles() {
        JsonArray vehicles = initialResources.get(0).getAsJsonObject().getAsJsonArray("vehicles");
        DeliveryVehicle[] outVehicles = new DeliveryVehicle[vehicles.size()];
        int i = 0;

        for (JsonElement item : vehicles) {
            DeliveryVehicle vehicle = new DeliveryVehicle(item.getAsJsonObject().get("license").getAsInt(), item.getAsJsonObject().get("speed").getAsInt());
            outVehicles[i] = vehicle;
            i++;
        }

        return outVehicles;


    }

    /**
     * @param initialInventory the book info array to set
     */
    public void setInitialInventory(BookInventoryInfo[] initialInventory) {
        this.initialInventory = initialInventory;
    }


    /**
     * @param services set the Services object of the program, which contains input
     *                 info on the services
     */
    public void setServices(Services services) {
        this.services = services;
    }

    /**
     * @return get the services object of the input file
     */
    public Services getServices() {
        return services;
    }
}
