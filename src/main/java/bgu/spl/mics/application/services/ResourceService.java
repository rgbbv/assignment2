package bgu.spl.mics.application.services;

import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.AcquireVehicleEvent;
import bgu.spl.mics.application.messages.ReleaseVehicleEvent;
import bgu.spl.mics.application.messages.TerminateBroadcast;
import bgu.spl.mics.application.passiveObjects.DeliveryVehicle;
import bgu.spl.mics.application.passiveObjects.ResourcesHolder;

/**
 * ResourceService is in charge of the store resources - the delivery vehicles.
 * Holds a reference to the {@link ResourceHolder} singleton of the store.
 * This class may not hold references for objects which it is not responsible for:
 * {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class ResourceService extends MicroService{

	private static int idCounter = 1;
	private ResourcesHolder resourcesHolder;

	public ResourceService() {
		super("Resource Service"+ idCounter);
		idCounter++;
		resourcesHolder = ResourcesHolder.getInstance();
	}

	/**
	 * subscribe to the relevant messages
	 */
	@Override
	protected void initialize() {
		subscribeEvent(AcquireVehicleEvent.class, event -> {
			Future<DeliveryVehicle> future = resourcesHolder.acquireVehicle();
			complete(event, future.get()); // returns the resolve of future given by ResourcesHolder
		});

		subscribeEvent(ReleaseVehicleEvent.class, event -> {
			resourcesHolder.releaseVehicle(event.getReleasedVehicle());
			complete(event, null); // there is no need for the resolve
		});
		subscribeBroadcast(TerminateBroadcast.class, broadcast -> this.terminate());

	}
}
