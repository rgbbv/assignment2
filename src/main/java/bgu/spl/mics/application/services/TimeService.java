package bgu.spl.mics.application.services;

import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.TerminateBroadcast;
import bgu.spl.mics.application.messages.TickBroadcast;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * TimeService is the global system timer There is only one instance of this micro-service.
 * It keeps track of the amount of ticks passed since initialization and notifies
 * all other micro-services about the current time tick using {@link TickBroadcast}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class TimeService extends MicroService{
	int speed; // number of milliseconds each clock tick takes
	int duration; // number of ticks before termination
	AtomicInteger counter; //amount of ticks passed

	public TimeService(int givenSpeed, int givenDuration) {
		super("Time Service");
		speed = givenSpeed;
		duration = givenDuration;
		counter = new AtomicInteger(1);
	}

	/**
	 * @return the speed in milliseconds of the clock tick
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the amount of ticks
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * send a tick broadcast in the correct time decided by speed and duration fields
	 */
	@Override
	protected void initialize() {
		while (counter.get() < duration) {
			// sends a broadcast with the amount of ticks left
			sendBroadcast(new TickBroadcast(counter.getAndIncrement()));
			try {
				Thread.sleep(speed); // waits until he needs to send another broadcast
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		//final tick, need to terminate gracefully, sending terminateEvent
		TerminateBroadcast event = new TerminateBroadcast();
		sendBroadcast(event);

		terminate();
	}
}
