package bgu.spl.mics.application.services;

import bgu.spl.mics.Callback;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.AcquireVehicleEvent;
import bgu.spl.mics.application.messages.DeliveryEvent;
import bgu.spl.mics.application.messages.ReleaseVehicleEvent;
import bgu.spl.mics.application.messages.TerminateBroadcast;
import bgu.spl.mics.application.passiveObjects.*;

/**
 * Logistic service in charge of delivering books that have been purchased to customers.
 * Handles {@link DeliveryEvent}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class LogisticsService extends MicroService {

	private static int idCounter = 0;

	public LogisticsService() {
		super("Logistics Service number: " + idCounter);
		idCounter++;
	}

	/**
	 * subscribe to the relevant messages
	 */
	@Override
	protected void initialize() {

		//subscribing to delivery event
		Callback<DeliveryEvent> deliveryCallBack = new DeliverCall();
		subscribeEvent(DeliveryEvent.class,deliveryCallBack);
		subscribeBroadcast(TerminateBroadcast.class, broadcast -> this.terminate());

	}

	/**
	 * callback for the delivery event, should get a vehicle, send it the delivery,
	 * and then release it
	 */
	private class DeliverCall implements Callback<DeliveryEvent> {
		@Override
		public void call(DeliveryEvent event) {

			String address = event.getAddress();
			int distance = event.getDistance();

			DeliveryVehicle vehicle = acquire(); //this is not a null vehicle
			if (vehicle == null){  //there is no vehicle, resource service is not working
				return;
			}

			vehicle.deliver(address,distance);

			release(vehicle); //releasing the vehicle
		}

		/**
		 * @param vehicle the vehicle to release
		 */
		private void release(DeliveryVehicle vehicle) {
			ReleaseVehicleEvent releaseEvent = new ReleaseVehicleEvent(vehicle);
			sendEvent(releaseEvent);

		}

		/**
		 * @return an acquired vehicle which can perform a delivery
		 */
		private DeliveryVehicle acquire(){

			AcquireVehicleEvent acquireEvent = new AcquireVehicleEvent();
			Future<DeliveryVehicle> vehicleFuture = sendEvent(acquireEvent);

			if (vehicleFuture == null){  //resource service already shut down, cant get any car
				return null;
			}

			DeliveryVehicle vehicle = vehicleFuture.get();

			if (vehicle == null){
				acquire();
			}

			return vehicle;

		}

	}


}
