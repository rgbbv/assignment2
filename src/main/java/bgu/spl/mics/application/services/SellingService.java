package bgu.spl.mics.application.services;

import bgu.spl.mics.Callback;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.*;
import bgu.spl.mics.application.passiveObjects.Inventory;
import bgu.spl.mics.application.passiveObjects.MoneyRegister;
import bgu.spl.mics.application.passiveObjects.OrderReceipt;
import bgu.spl.mics.application.passiveObjects.ResourcesHolder;
import javafx.util.Pair;

/**
 * Selling service in charge of taking orders from customers.
 * Holds a reference to the {@link MoneyRegister} singleton of the store.
 * Handles {@link BookOrderEvent}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class SellingService extends MicroService{

	private MoneyRegister moneyRegister;
	private static int idcounter = 0;  //static idcounter to give different id for each service
	private int recentTick;


	/**
	 * constructor for this microservice
	 */
	public SellingService() {
		super("Selling Service number: " + idcounter);
		idcounter++;
		moneyRegister = MoneyRegister.getInstance();
	}

	/**
	 * overridden method that will be called when this microService is initialized
	 * subscribing to all relevant messages
	 */
	@Override
	protected void initialize() {
		Callback<TickBroadcast> tickCallback = new TickCallback();
		subscribeBroadcast(TickBroadcast.class, tickCallback ); //subscribing to tick broadcast

		Callback<BookOrderEvent> bookOrderCallback = new BookOrderCallBack();
		subscribeEvent(BookOrderEvent.class, bookOrderCallback); //subscribing to book order event

		subscribeBroadcast(TerminateBroadcast.class, (br) ->
				this.terminate()); //subscribing to terminate service;
		
	}

	/**
	 * the callback function of selling service that should be called when receiving a tick broadcast.
	 * updates the recentTick field of this microservice with the latest tick got for the broadcast.
	 */
	private class TickCallback implements Callback<TickBroadcast>{
		@Override
		public void call(TickBroadcast b) {
			recentTick = b.getTick();
		}
	}

	/**
	 * the callback function of selling service thar should be alled when receiving a bookOrder event.
	 * checks if the book is avialable in inventory by creating a checkaviability event and
	 * sending it to the message bus.
	 * if the book is not available then return null.
	 * if available then if the customer has enough money, then charge the customer and add the money to the register
	 * , send a removeBook event, and return the receipt via the complete() method. also add the receipt to the register
	 *
	 * if customer dont have enough money, return null.
	 */
	private class BookOrderCallBack implements Callback<BookOrderEvent>{

		/**
		 * the callback function of SellingService for BookOrder event
		 * @param e the bookOrder event that should be worked on
		 */
		@Override
		public void call(BookOrderEvent e) {
			OrderReceipt receipt = new OrderReceipt(e.getCustomer().getId(),e.getBookName(),e.getOrderTick(),recentTick, getName());
			AvailabilityAndPriceEvent inventoryCheck = new AvailabilityAndPriceEvent(e.getBookName()); //checking availabilty and price of book

			synchronized (e.getCustomer()) {
				Future<Pair<Boolean, Integer>> futureCheck = sendEvent(inventoryCheck);
				Pair<Boolean, Integer> availAndPrice = futureCheck.get();

				if (availAndPrice.getKey()) { //book is available
					int price = availAndPrice.getValue();

					if (e.getCustomer().getAvailableCreditAmount() < price) {//not enough money for customer
						complete(e, null); //no receipt cause book wasn't bought
					}

					else {  //enough money
						moneyRegister.chargeCreditCard(e.getCustomer(), price);
						receipt.setPrice(price);
						RemoveBookEvent removeBook = new RemoveBookEvent(e.getBookName()); //creating new removebook event
						sendEvent(removeBook); //sending an event to remove a copy of the bought book from the inventory
						receipt.setIssuedTick(recentTick);
						complete(e, receipt);
						moneyRegister.file(receipt);
					}

				} else { //book is not available
					complete(e, null); //no receipt cause book is not available so was not bought
				}
			}

		}
	}

}
