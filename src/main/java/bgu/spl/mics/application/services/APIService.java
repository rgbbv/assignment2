package bgu.spl.mics.application.services;

import bgu.spl.mics.Callback;
import bgu.spl.mics.Future;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.BookOrderEvent;
import bgu.spl.mics.application.messages.DeliveryEvent;
import bgu.spl.mics.application.messages.TerminateBroadcast;
import bgu.spl.mics.application.messages.TickBroadcast;
import bgu.spl.mics.application.passiveObjects.*;

import java.util.Collections;
import java.util.List;

/**
 * APIService is in charge of the connection between a client and the store.
 * It informs the store about desired purchases using {@link BookOrderEvent}.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}, {@link Inventory}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */
public class APIService extends MicroService{

	private static int idCounter = 1;
	private List<Customer.CustomerOrder> orderSchedule;
	private int orderindex = 0;  //index of the next order from schedule that should be sent

	public APIService(List<Customer.CustomerOrder> orderSchedule) {
		super("API Service number: " + idCounter);
		idCounter++;

		Collections.sort(orderSchedule); //sorting the list by ticks
		this.orderSchedule = orderSchedule;
	}

	/**
	 * subscribe to the relevant messages
	 */
	@Override
	protected void initialize() {
		Callback<TickBroadcast> tickCallback = new TickCallback();
		subscribeBroadcast(TickBroadcast.class, tickCallback ); //subscribing to tick broadcast
		subscribeBroadcast(TerminateBroadcast.class, broadcast ->this.terminate());
	}



	/**
	 * the callback function of selling service that should be called when receiving a tick broadcast.
	 * send a BookOrderEvent if the schedule contains an order with the same tick received from the broadcast.
	 */
	private class TickCallback implements Callback<TickBroadcast>{
		@Override
		public void call(TickBroadcast b) {
			int recentTick = b.getTick();

			//loop in 'while' because some orders have the same tick
			while (orderindex < orderSchedule.size() && orderSchedule.get(orderindex).getTick() == recentTick) {  //its the time to send the order for this tick
				String bookTitle = orderSchedule.get(orderindex).getBookTitle();
				Customer customer = orderSchedule.get(orderindex).getCustomer();


				BookOrderEvent event = new BookOrderEvent(bookTitle,customer,recentTick);
				Future<OrderReceipt> futureReceipt = sendEvent(event);  //sending the bookorder event

				if (futureReceipt == null){ //could not buy book, return null
                                    orderindex++;
                                    continue;
                                }
				
				OrderReceipt receipt = futureReceipt.get();
				if (receipt == null){  //receipt is null, the customer could not buy the book
					orderindex++;
					continue;
				}

				//order is successful!
				customer.addReceipt(receipt);
				orderindex++;

				//send a delivery event
				String address = customer.getAddress();
				int distance = customer.getDistance();

				DeliveryEvent deliveryEvent = new DeliveryEvent(address,distance);
				sendEvent(deliveryEvent);
			}


		}
	}


}
