package bgu.spl.mics.application.services;

import bgu.spl.mics.Callback;
import bgu.spl.mics.MicroService;
import bgu.spl.mics.application.messages.AvailabilityAndPriceEvent;
import bgu.spl.mics.application.messages.RemoveBookEvent;
import bgu.spl.mics.application.messages.TerminateBroadcast;
import bgu.spl.mics.application.passiveObjects.Inventory;
import bgu.spl.mics.application.passiveObjects.MoneyRegister;
import bgu.spl.mics.application.passiveObjects.OrderResult;
import bgu.spl.mics.application.passiveObjects.ResourcesHolder;
import javafx.util.Pair;

/**
 * InventoryService is in charge of the book inventory and stock.
 * Holds a reference to the {@link Inventory} singleton of the store.
 * This class may not hold references for objects which it is not responsible for:
 * {@link ResourcesHolder}, {@link MoneyRegister}.
 * 
 * You can add private fields and public methods to this class.
 * You MAY change constructor signatures and even add new public constructors.
 */

public class InventoryService extends MicroService {

	private static int idCounter = 1; //the number of this instance
	private Inventory inventory;

	public InventoryService() {
		super("Inventory Service "+idCounter);
		idCounter++;
		inventory = Inventory.getInstance();
	}

	/**
	 * subscribe to the relevant messages
	 */
	@Override
	protected void initialize() {

		subscribeEvent(AvailabilityAndPriceEvent.class , new AvailabilityCallBack()); //subscribing to relevent event

		subscribeEvent(RemoveBookEvent.class, event ->
		{
			OrderResult status = inventory.take(event.getBookTitle());
			complete(event,status);
		});
		subscribeBroadcast(TerminateBroadcast.class, (br) -> this.terminate());


	}

	private class AvailabilityCallBack implements Callback<AvailabilityAndPriceEvent> {

		/**
		 * the callback for availabiltyandprice event, checks if the book is available
		 */
		public AvailabilityCallBack () {}

		@Override
		public void call(AvailabilityAndPriceEvent event) {
			Boolean result = false;
			Integer price = 0;
//
			price = inventory.checkAvailabiltyAndGetPrice(event.getBookName());
			if (price > 0){
				result = true;
			}

			complete(event, new Pair<>(result, price));
		}
	}
}
