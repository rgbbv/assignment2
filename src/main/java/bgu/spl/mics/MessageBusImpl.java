package bgu.spl.mics;

import bgu.spl.mics.application.messages.TerminateBroadcast;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * The {@link MessageBusImpl class is the implementation of the MessageBus interface.
 * Write your implementation here!
 * Only private fields and methods can be added to this class.
 */
public class MessageBusImpl implements MessageBus {

	private ConcurrentHashMap<Class<? extends Event<?>>, ConcurrentLinkedQueue<MicroService>> eventToMicroserviceQueue;
	private ConcurrentHashMap<Class<? extends Broadcast>,ConcurrentLinkedQueue<MicroService>> broadcastToMicroserviceQueue;
	private ConcurrentHashMap<Event<?>, Future<?>> eventToFuture; //hashmap to keep track of which future belong to which event
	private ConcurrentHashMap<MicroService, ConcurrentLinkedDeque<Message>> microserviceToMessageQueue; //map for microservices to wait for messages

	private static class instanceHolder {
		private static MessageBusImpl instance = new MessageBusImpl();
	}


	/**
	 * The only constructor for this class.
	 * can only be called once by getInstance().
	 */
	private MessageBusImpl() {
		eventToMicroserviceQueue = new ConcurrentHashMap<>();
		broadcastToMicroserviceQueue = new ConcurrentHashMap<>();
		eventToFuture = new ConcurrentHashMap<>();
		microserviceToMessageQueue = new ConcurrentHashMap<>();
	}


	/**
	 * @return the single instance of the messageBus
	 */
	public static MessageBusImpl getInstance(){
		return instanceHolder.instance;
	}


	/**
	 * Subscribes {@code m} to receive {@link Event}s of type {@code type}.
	 * <p>
	 * @param <T>  The type of the result expected by the completed event.
	 * @param type The type to subscribe to,
	 * @param m    The subscribing micro-service.
	 */
	@Override
	public <T> void subscribeEvent(Class<? extends Event<T>> type, MicroService m) {
		synchronized (this) {
			if (!eventToMicroserviceQueue.containsKey(type)) { //no existing event type, creating new one
				ConcurrentLinkedQueue<MicroService> queue = new ConcurrentLinkedQueue<>();
				queue.add(m);
				eventToMicroserviceQueue.put(type, queue);
				return;
			}
		}
		//else, queue exists for this event type, adding the service

		eventToMicroserviceQueue.get(type).add(m);
	}


	/**
	 * Subscribes {@code m} to receive {@link Broadcast}s of type {@code type}.
	 * <p>
	 * @param type 	The type to subscribe to.
	 * @param m    	The subscribing micro-service.
	 */
	@Override
	public void subscribeBroadcast(Class<? extends Broadcast> type, MicroService m) {
		synchronized (this) {
			if (!broadcastToMicroserviceQueue.containsKey(type)) {//no existing broadcast type, creating new one
				ConcurrentLinkedQueue<MicroService> queue = new ConcurrentLinkedQueue<>();
				queue.add(m);
				broadcastToMicroserviceQueue.put(type, queue);

				return;
			}
		}
		//else, queue already exists, adding service to it
		broadcastToMicroserviceQueue.get(type).add(m);
	}

	/**
	 * Notifies the MessageBus that the event {@code e} is completed and its
	 * result was {@code result}.
	 * When this method is called, the message-bus will resolve the {@link Future}
	 * object associated with {@link Event} {@code e}.
	 * <p>
	 * @param <T>    The type of the result expected by the completed event.
	 * @param e      The completed event.
	 * @param result The resolved result of the completed event.
	 */
	@Override
	public <T> void complete(Event<T> e, T result) {
		Future<T> future = (Future<T>) eventToFuture.get(e);
		future.resolve(result);
	}

	/**
	 * Adds the {@link Broadcast} {@code b} to the message queues of all the
	 * micro-services subscribed to {@code b.getClass()}.
	 * <p>
	 * @param b 	The message to added to the queues.
	 */
	@Override
	public void sendBroadcast(Broadcast b) {

		if (b instanceof TerminateBroadcast){
			for (MicroService microService : broadcastToMicroserviceQueue.get(b.getClass())) { //iterate over the queue of microservices that are subscribed for the broadcast type
				microserviceToMessageQueue.get(microService).addFirst(b); //add the message to the queue of subscribed microservice
				synchronized (this) {
					notifyAll();
				}
			}

			return;
		}

		if (!broadcastToMicroserviceQueue.containsKey(b.getClass())){ //no microservice that is subscribed to this broadcast
			return;
		}
		else {
			for (MicroService microService : broadcastToMicroserviceQueue.get(b.getClass())) { //iterate over the queue of microservices that are subscribed for the broadcast type
				microserviceToMessageQueue.get(microService).addLast(b); //add the message to the queue of subscribed microservice
			}
			synchronized (this) {
				notifyAll();
			}
		}
	}

	/**
	 * Adds the {@link Event} {@code e} to the message queue of one of the
	 * micro-services subscribed to {@code e.getClass()} in a round-robin
	 * fashion. This method should be non-blocking.
	 * <p>
	 * @param <T>    	The type of the result expected by the event and its corresponding future object.
	 * @param e     	The event to add to the queue.
	 * @return {@link Future<T>} object to be resolved once the processing is complete,
	 * 	       null in case no micro-service has subscribed to {@code e.getClass()}.
	 */
	@Override
	public <T> Future<T> sendEvent(Event<T> e) {
		if (!eventToMicroserviceQueue.containsKey(e.getClass())) { //no microservice that is subscribed to this event
			return null;
		}


		Future<T> future = new Future<T>();
		eventToFuture.put(e, future);  //adding the future to map for later to check if resolved

		synchronized (this) {
			MicroService worker = eventToMicroserviceQueue.get(e.getClass()).poll(); //getting to microservice in head of queue
			if (worker == null || !microserviceToMessageQueue.containsKey(worker)){
				return null;
			}

			microserviceToMessageQueue.get(worker).addLast(e); //add the event message to the queue of the subscribed microservice

			notifyAll(); //notifying a message was added to the queue, so microservices should check agaiin if they got a message

			eventToMicroserviceQueue.get(e.getClass()).add(worker);  //pushing the microservice back to the queue to support round-robin choosing
		}
		return future;

	}

	/**
	 * move the {@link Event} {@code e} from an unregistred microserivce to the message queue of one of the
	 * micro-services subscribed to {@code e.getClass()} in a round-robin
	 * fashion. This method should be non-blocking.
	 * if there is no other micro-services that are subscribed, then resolve the associated Future-Object to NULL
	 * <p>
	 * @param e    The event to move to the queue.
	 */
	private synchronized void moveEvent(Event<?> e){
		if (!eventToMicroserviceQueue.containsKey(e.getClass()) || eventToMicroserviceQueue.get(e.getClass()).isEmpty()) { //no microservice that is subscribed to this event
			complete(e,null); //resolving to null
			return;
		}

		MicroService worker = eventToMicroserviceQueue.get(e.getClass()).poll(); //getting to microservice in head of queue
		

		microserviceToMessageQueue.get(worker).add(e); //add the event message to the queue of the subscribed microservice

		notifyAll(); //notifying a message was added to the queue, so microservices should check agaiin if they got a message


		eventToMicroserviceQueue.get(e.getClass()).add(worker);  //pushing the microservice back to the queue to support round-robin choosing


	}


	/**
	 * Allocates a message-queue for the {@link MicroService} {@code m}.
	 * <p>
	 * @param m the micro-service to create a queue for.
	 */
	@Override
	public void register(MicroService m) {
		microserviceToMessageQueue.put(m,new ConcurrentLinkedDeque<Message>());

	}


	/**
	 * Removes the message queue allocated to {@code m} via the call to
	 * {@link #register(bgu.spl.mics.MicroService)} and cleans all references
	 * related to {@code m} in this message-bus. If {@code m} was not
	 * registered, nothing should happen.
	 * <p>
	 * @param m the micro-service to unregister.
	 */
	@Override
	public synchronized void unregister(MicroService m) {
		if (!microserviceToMessageQueue.containsKey(m)){ //no microservice to unregister
			return;
		}

		//clearing the microservice from all events that are subscribed to it
		for (Class<? extends Event> event : eventToMicroserviceQueue.keySet()){  //iterating over the Event-Type keys in the map
			for (MicroService service : eventToMicroserviceQueue.get(event)){ //iterating over the microservice queue associated to the Event-Type
				if (service == m){ //removing the service if its the one that should be unregistered
					eventToMicroserviceQueue.get(event).remove(m);
				}
			}

		}


		//clearing the microservice from all broadcasts that are subscribed to it
		for (Class<? extends Broadcast> broadcast: broadcastToMicroserviceQueue.keySet()){ //iterating over the Broadcast-Type keys in the map
			for (MicroService service : broadcastToMicroserviceQueue.get(broadcast)){ //iterating over the microservices queue associated to the broadcast-type
				if (service == m){ //removing the service if its the one that should be unregistered
					broadcastToMicroserviceQueue.get(broadcast).remove(m);
				}
			}
		}

		//move unproccesed events from microservice m (that should be unregsitred) to other microservices
		for (Message message: microserviceToMessageQueue.get(m)){ //iterating over the messages that unregsitred m is waiting to get, and move them to other services
			if (message instanceof Event){  //if the message is an event
				moveEvent((Event<?>) message); //move the event to another microservice's message-queue if possible, else resolve the associated Future object to NULL
			}
		}

		microserviceToMessageQueue.remove(m); //clearing microservice from the messagesqueue map


	}


	/**
	 * Using this method, a <b>registered</b> micro-service can take message
	 * from its allocated queue.
	 * This method is blocking meaning that if no messages
	 * are available in the micro-service queue it
	 * should wait until a message becomes available.
	 * The method should throw the {@link IllegalStateException} in the case
	 * where {@code m} was never registered.
	 * <p>
	 * @param m The micro-service requesting to take a message from its message
	 *          queue.
	 * @return The next message in the {@code m}'s queue (blocking).
	 * @throws InterruptedException if interrupted while waiting for a message
	 *                              to became available.
	 */
	@Override
	public Message awaitMessage(MicroService m) throws InterruptedException {
		if (!microserviceToMessageQueue.containsKey(m)){
			throw new IllegalStateException("given microservice is not yet registred");
		}
		ConcurrentLinkedDeque<Message> messageQueue = microserviceToMessageQueue.get(m);
		while (messageQueue.isEmpty() ){
			synchronized(this) {
				wait();
			}
		}
		return messageQueue.poll();

	}

	

}
