package bgu.spl.mics;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * A Future object represents a promised result - an object that will
 * eventually be resolved to hold a result of some operation. The class allows
 * Retrieving the result once it is available.
 * 
 * Only private methods may be added to this class.
 * No public constructor is allowed except for the empty constructor.
 */
public class Future<T> {
	T result = null;
	boolean isResolved = false;  //flag to notify if the future holds a resolved result (which could be null)

	/**
	 * This should be the the only public constructor in this class.
	 */
	public Future() {
	}
	
	/**
     * retrieves the result the Future object holds if it has been resolved.
     * This is a blocking method! It waits for the computation in case it has
     * not been completed.
     * <p>
     * @return return the result of type T if it is available, if not wait until it is available.
     * 	       
     */
	public synchronized T get() {
		while (!isDone()){  //result has not been resolved

			try {
				this.wait();  //block the thread until answer is resolved

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return this.result;
	}
	
	/**
     * Resolves the result of this Future object.
     */
	public void resolve (T result) {
		this.result = result;
		isResolved = true;
		synchronized (this) {
			notifyAll();
		}
	}
	
	/**
     * @return true if this object has been resolved, false otherwise
     */
	public boolean isDone() {


		return isResolved;
	}
	
	/**
     * retrieves the result the Future object holds if it has been resolved,
     * This method is non-blocking, it has a limited amount of time determined
     * by {@code timeout}
     * <p>
     * @param timeout 	the maximal amount of time units to wait for the result.
     * @param unit		the {@link TimeUnit} time units to wait.
     * @return return the result of type T if it is available, if not, 
     * 	       wait for {@code timeout} TimeUnits {@code unit}. If time has
     *         elapsed, return null.
     */
	public synchronized T get(long timeout, TimeUnit unit) {
		Date date = new Date();

		long timeoutInMillis = unit.toMillis(timeout);
		long nowInMillis = date.getTime();
		long endTimeInMillis = nowInMillis + timeoutInMillis;;

		while (!isDone()){ //while is not resolved
			try {
				wait(timeoutInMillis);
				nowInMillis = new Date().getTime();
				if (nowInMillis > endTimeInMillis) { // if timeout has passed, resolve with null
					isResolved = true;
					return null;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return this.result;
	}

}
