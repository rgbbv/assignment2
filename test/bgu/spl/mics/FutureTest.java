package bgu.spl.mics;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class FutureTest {

    private Future<Object> myFuture;
    private boolean isResolved;

    @Before
    public void setUp() throws Exception {
        myFuture = new Future<>();

    }


    @Test
    public void get() {
        try {
            myFuture.resolve(new String("test"));
            assertEquals("test", myFuture.get());
        }
        catch (Exception ex){
            fail("shouldnt throw exepctions");
        }


    }

    @Test
    public void resolve() {
        try {
            myFuture.resolve(new String("test"));
            assertEquals("test", myFuture.get());
        }
        catch (Exception ex){
            fail("shouldnt throw exepctions");
        }
    }

    @Test
    public void isDone() {
        try {
            assertFalse(myFuture.isDone()); //object not resolved so should return false

            myFuture.resolve(new String("test")); //resolving some result
            assertTrue(myFuture.isDone()); //result us resolved therefore should return true
        }
        catch (Exception ex){
            fail("shouldnt throw exepctions");
        }
    }

    @Test
    public void get1() {
        try {
            assertEquals(null, myFuture.get(1, TimeUnit.NANOSECONDS));

            myFuture.resolve(new String("test"));
            assertEquals("test", myFuture.get(1, TimeUnit.NANOSECONDS));
        }
        catch (Exception ex){
            fail("shouldnt throw exepctions");
        }


    }
}