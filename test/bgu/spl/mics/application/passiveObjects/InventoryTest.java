package bgu.spl.mics.application.passiveObjects;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;

import static org.junit.Assert.*;

public class InventoryTest {
    private Inventory myInventory;

    /*@Before
    public void setUp() throws Exception {
        myInventory = new Inventory();
    }*/

    @Test
    public void getInstance() {
        try {
            assertFalse(Inventory.getInstance() == null); //will never return null

            assertEquals(Inventory.getInstance(), Inventory.getInstance());
        }
        catch (Exception ex){
            fail("shouldnt throw exceptions");
        }



    }

    @Test
    public void load() {

        try{
            BookInventoryInfo book1 = new BookInventoryInfo("book1",55,3);
            BookInventoryInfo book2 = new BookInventoryInfo("book2",40,5);

            BookInventoryInfo[] books = {book1,book2};



            myInventory = Inventory.getInstance();

            assertEquals(myInventory.take("book1"), OrderResult.NOT_IN_STOCK);

            myInventory.load(books);


            assertEquals(myInventory.take("book1"), OrderResult.SUCCESFULLY_TAKEN);
            assertEquals(myInventory.take("book5"), OrderResult.NOT_IN_STOCK);



        } catch (Exception ex){
            fail("method shouldnt throw exceptions");
        }


    }

    @Test
    public void take() {
        try {
            BookInventoryInfo book1 = new BookInventoryInfo("book1", 55, 3);
            BookInventoryInfo book2 = new BookInventoryInfo("book2", 40, 5);

            BookInventoryInfo[] books = {book1, book2};

            myInventory = Inventory.getInstance();
            myInventory.load(books);


            for (int i = 0; i < 5; i++) {
                assertEquals(myInventory.take("book2"), OrderResult.SUCCESFULLY_TAKEN);

            }
            assertEquals(myInventory.take("book2"), OrderResult.NOT_IN_STOCK);
        } catch (Exception ex){
            fail("method shouldnt throw exceptions");
        }


    }

    @Test
    public void checkAvailabiltyAndGetPrice() {
        try {
            BookInventoryInfo book1 = new BookInventoryInfo("book1", 55, 3);
            BookInventoryInfo book2 = new BookInventoryInfo("book2", 40, 5);

            BookInventoryInfo[] books = {book1, book2};

            myInventory = Inventory.getInstance();

            assertEquals(-1, myInventory.checkAvailabiltyAndGetPrice("book1"));

            myInventory.load(books);

            assertEquals(-1, myInventory.checkAvailabiltyAndGetPrice("book5"));
            assertEquals(55, myInventory.checkAvailabiltyAndGetPrice("book1"));
        } catch (Exception ex){
            fail("method shouldnt throw exceptions");
        }

    }

    @Test
    public void printInventoryToFile() {
        try {

            BookInventoryInfo book1 = new BookInventoryInfo("book1", 55, 3);
            BookInventoryInfo book2 = new BookInventoryInfo("book2", 40, 5);

            BookInventoryInfo[] books = {book1, book2};

            myInventory = Inventory.getInstance();
            myInventory.load(books);

            myInventory.printInventoryToFile("HashMap.ser");

            FileInputStream inputStream = new FileInputStream("HashMap.ser");
            ObjectInputStream objectStream = new ObjectInputStream(inputStream);
            HashMap<String, Integer> map = (HashMap<String, Integer>) objectStream.readObject();

            assertEquals(new Integer(3), map.get("book1"));

        } catch (FileNotFoundException ex){
            fail("shouldnt throw this exception, file is there");


        } catch (Exception ex) {
            ex.printStackTrace();
            fail("shouldnt throw exceptions");
        }

    }
}